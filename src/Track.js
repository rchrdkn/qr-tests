import React, { Component } from 'react'

class Track extends Component {

  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    const target = e.target
    let value = target.value

    if (-1 !== [ 'range', 'number' ].indexOf(target.type)) {
      value = parseInt(value)
    }

    this.props.onChange({
      name: this.props.name,
      value: value,
    })
  }

  render() {
    return (
      <div style={{ marginBottom: '30px' }}>

        <label style={{
          display: 'block',
          marginBottom: '10px'
        }}>
          {this.props.label}
        </label>

        <input
          type="number"
          min="0"
          max="100"
          value={this.props.speed}
          onChange={this.handleChange}
          style={{
            display: 'inline-block',
            verticalAlign: 'middle',
            marginRight: '10px'
          }}
        />

        <input
          type="range"
          min="0"
          max="100"
          value={this.props.speed}
          onChange={this.handleChange}
          style={{
            display: 'inline-block',
            verticalAlign: 'middle',
            marginRight: '10px'
          }}
        />
      </div>
    )
  }

}

export default Track
