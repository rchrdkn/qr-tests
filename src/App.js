import React, { Component } from 'react'
import { w3cwebsocket as WebSocket } from 'websocket'
import { debounce } from 'lodash'
import Track from './Track'

const uri = 'ws://192.168.247.93:9000'
// const uri = 'ws://127.0.0.1:9000'

const client = new WebSocket(uri, null);

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      track1: { speed: 0 },
      track2: { speed: 0 },
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleCalibrate = this.handleCalibrate.bind(this)

    this.notifyWebSocket = debounce(this.notifyWebSocket, 500)
  }

  componentDidMount() {
    client.onopen = () => {
      console.log('WebSocket Client Connected')
    }

    client.onmessage = (message) => {
      console.log(message)
    }
  }

  handleChange({name, value}) {
    this.setState({ [name]: { speed: value } })
    this.notifyWebSocket({ [name + 'Speed']: value })
  }

  notifyWebSocket(data) {
    client.send(JSON.stringify(data))
  }

  handleCalibrate() {
    this.notifyWebSocket({ calibrate: true })
  }

  render() {
    return (
      <div className="app">
        <Track
          name="track1"
          label="Track 1"
          speed={this.state.track1.speed}
          onChange={this.handleChange}
        />

        <Track
          name="track2"
          label="Track 2"
          speed={this.state.track2.speed}
          onChange={this.handleChange}
        />

        <button
          onClick={this.handleCalibrate}
        >
          Calibrate
        </button>
      </div>
    )
  }

}

export default App
